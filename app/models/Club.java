package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;

import javax.persistence.Entity;
import play.db.jpa.Model;

@Entity
public class Club extends Model {
	public String name;

	public Club(String name) {
		this.name = name;
		this.players = new ArrayList<Player>();
	}

	 public void addPlayer(Player player)
	   {
	     player.club = this;
	     players.add(player);
	   }

	public static Club findByName(String name) {
		return find("name", name).first();
	}

	public String toString() {
		return name;
	}

	@OneToMany(mappedBy="club", cascade=CascadeType.ALL)
	   public List<Player> players;
}